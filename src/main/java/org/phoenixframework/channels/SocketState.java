package org.phoenixframework.channels;

public enum SocketState {
    CONNECTING,
    OPEN,
    CLOSING,
    CLOSED
}
